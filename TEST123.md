Percobaan Gerak
========================================================

Untuk Video Percobaan bisa dilihat di link berikut ini:

1. [CekTangan](https://drive.google.com/file/d/1eIUC6zINEg_hYJYxy6zthSyJPdYF0XCg/view)
2. [CekRoda](https://drive.google.com/file/d/1934L3GUKz8lHVo25m3jVi-fubJfVsxJI/view)
3. [CekCamera](https://drive.google.com/file/d/1Wq-HPz9HpFU_vVuP8TRa8O8T5kK9saiZ/view)


Percobaan Awal sebelum melakukan improvisasi selanjutnya.
