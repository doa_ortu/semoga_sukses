# Semoga_Sukses
Dengan doa orang tua yang mengiringi, kesuksesan didepan mata.<br>
Oleh :<br>
Nama : Fitrah Arie Ramadhan <br>
NRP  : 05111940000025 <br> <br>
Nama : Ikhlasul Amal Rivel <br>
NRP  : 05111940000145 <br> <br>
Nama : Nathanael Hutama Harsono <br>
NRP  : 07211940000044 <br> 
 
## Konsep ROLUNG (Robot Pemulung)
====================================================

**A. Bentuk Fisik**
RoLung ini berbentuk seperti robot pada film fiksi yaitu WALL•E.
1. Memiliki badan kubus, 
2. Dua buah roda rantai sebagai penggerak untuk mengubah posisi, 
3. Mempunyai sepasang tangan dan 
4. Dua buah mata (kamera) sebagai visual.

**B. Sensor**
RoLung ini memiliki sensor untuk dapat membantu mencapai tujuannya.
1. Color Sensor, berguna untuk mengenali bentuk benda melalui warna.
2. Distance Sensor, berguna untuk menentukan barang yang akan diambil.

**C. Teori**
1. OpenCV, digunakan untuk mengenali warna melalui color sensor.
2. Webots, digunakan untuk membuat prototipe robot.
3. Code untuk mapping RoLung

**D. Cara Kerja**
1. RoLung memiliki informasi awal berupa letak dari tempat sampah
2. RoLung juga memiliki informasi awal berupa warna dari sampah yaitu (merah, hijau, biru), warna dinding, dan warna rintangan
3. Sampah dan rintangan bisa berupa bentuk apa saja
4. RoLung yang malang akan mencari nafkah dengan mengumpulkan sampah sesuai warnanya lalu diletakkan pada tempat sampah yang sesuai
5. Selagi RoLung berkelana, dia juga memetakan wilayah agar memastikan sampah di daerah itu telah diambil
6. Setelah semua wilayah benar-benar bebas dari sampah, RoLung akan kembali ke titik semula
